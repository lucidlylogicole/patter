# Patter
Sometimes only a simple server is needed for simple projects.  Patter is a single-file, light-weight http/https server for [Nodejs](https://nodejs.org/) that doesn't have any dependencies. 

Patter provides basic routing, file handling, and data handling. For more functionality, the standard Nodejs [http](https://nodejs.org/api/http.html) module that patter is built on can be utilized.

------------------------------------------------------------------------

## Installation
1. Download [patter.js](https://gitlab.com/lucidlylogicole/patter/-/raw/master/patter.js) to a directory.

## Usage

    const { PatterServer } = require("./patter.js");
    app = new PatterServer()
    app.route('/',function(req,resp){
        resp.send('hello world')
    })
    app.run()


------------------------------------------------------------------------

## PatterServer(options)
The main server app object

    app = new PatterServer({port:8080})


- **options**
    - `port` _(int)_ - specify the port to use
    - `ssl` _(object)_ - optionally use a https server. See the "HTTPS Server with SSL" section below for more details on setting up a HTTPS Server
        - `key` - contents of key file
        - `cert` - contents of cert file

## Routes
PatterServer has basic route handling that passes the request and response objects

**app.route( '/', (req, resp) => {} )**

    app.route( '/', (req, resp) => {} )
    app.route( '/example', (req, resp) => {} )

### Route Methods
Currently PatterServer does not handle routes with different methods. That logic must be handled in the main function for that route.

    app.route( '/form', (req, resp) => {
        if (req.method == 'GET') {
            ...
        } else if (req.method == 'POST') {
            ...
        }
    })

### Routes with Arguments
PatterServer supports basic arguments in the route and will return an array of the values in the same order.  The name of the argument is not used for `req.args` but available instead with `req.route`.

**req.args** - provides an array of the argument value(s)

    app.route( '/user/:userid', (req, resp) => {
        user_id = req.args[0]
    })
    // for '/user/4654', req.args equals ['4654']

Multiple Arguments

    app.route( '/date/:year/:month', (req, resp) => {
        year = req.args[0]
        month = req.args[1]
    })
    // for '/date/2018/10', req.args equals ['2018', '10']



### Routes with Wildcard argument
The `::` is a wildcard argument that will pass the rest of the url as an argument.  This is useful for static files.

    app.route('/static/::', (req, resp) => {} )
    // for '/static/jquery/jquery.js', req.args equals ['jquery/jquery.js']

Arguments can be used along with the wildcard argument.  The wildcard argument must come last.

    app.route('/static/:myfolder/::', (req, resp) => {} )
    // for '/static/jquery/jquery.js', req.args equals ['jquery', 'jquery.js']

## Request Route Variables
Instead of getting the req.args list, the req.route object will return the route as keys with the args as values.

**req.route** - provides dictionary of route with arg values

    app.route( '/date/:year/:month', (req, resp) => {
        year = req.route.year
        month = req.route.month
    })
    // for '/date/2018/10', req.route equals {year:'2018', month:'10'}

For the wildcard argument at the end, the value is placed with the `end` key

    app.route('/static/:myfolder/::', (req, resp) => {} )
    // for '/static/jquery/jquery.js', req.route equals [myfolder:'jquery', end:'jquery.js']

## Query / Search Parameters

Any query parameters from the url are available in the request.query object, which is a reference to the node [url.searchParams](https://nodejs.org/api/url.html#url_url_searchparams)

    // for url http://127.0.0.1/users?name=Dwight
    req.query.name // equals 'Dwight'

## Request.URL
For convenience the request object in PatterServer references the node [url](https://nodejs.org/api/url.html) module object as

    req.URL

## Getting Form Data

**req.getFormData()** - get data from the request

    app.route('/form', (req,resp) => {
        if (req.method == "POST") {
            req.getFormData().then(data=>{
                JSON.stringify(data)
                resp.send('ok')
            })
        }
    })

## Generating a Response
To generate a reponse, PatterServer utilizes the [http.ServerReponse](https://nodejs.org/api/http.html#http_class_http_serverresponse) object, which is passed to the route function.

**resp.send( 'string' )** - Send a simple response with `resp.send` (which just calls  [resp.end](https://nodejs.org/api/http.html#http_response_end_data_encoding_callback))
:

    app.route('/', (req, resp) => {
        resp.send('string')
    })


Some of the useful response functions:
- response.setHeader(name,value) - set a header value [docs](https://nodejs.org/api/http.html#http_response_setheader_name_value)
- response.write(chunk[, encoding][, callback]) - write data [docs](https://nodejs.org/api/http.html#http_response_write_chunk_encoding_callback)
- response.end([data[, encoding]][, callback]) [docs](https://nodejs.org/api/http.html#http_response_end_data_encoding_callback)

__*Note: Always end a response with `resp.end()`*__

## JSON Response
To generate a JSON response, set the header and send a JSON string.

    app.route('/json', (req, resp) => {
        resp.setHeader("content-type", "application/json")
        resp.end(JSON.stringify({text:'hi'}))
    })

or use the 

**resp.json ( obj )** - send an object as JSON

    app.route('/json', (req, resp) => {
        resp.json({text:'hi'})
    })

## Send File / Static Files

**resp.sendFile(root_dir, filename,)** - send a local file

    app.route('/static/::', (req, resp) => {
        resp.sendFile(require('path').join(__dirname,'static'), req.args[0] )
    })

- sendFile will read the file and pass it to the response.
- root directory is specified to avoid directory traversal attacks
- automatically sets headers for basic mime types (see PatterServer.mimeTypes)
- points to index.html if a directory location is passed

## Redirect

**resp.redirect( redirect_url )** - redirect response

    app.route('/redirect', (req,resp) => {
        resp.redirect( '/' )
    })

## Cookies

**resp.setCookies(cookies)** - set one or more cookies

    resp.setCookies({user:'bob'})


**req.getCookies()** - get cookies from the request

    req.getCookies()  // returns {user:'bob'}

## HTTPS Server with SSL
Patter can be run with https using a SSL certificate.

To generate a self signed certificate:

    openssl req -nodes -new -x509 -keyout mycert.key -out mycert.cert

Add an ssl section to the options when starting patter:

    let opt = {
      port:443,   // optionally set the port
      ssl:{
        key:fs.readFileSync('mycert.key'),
        cert:fs.readFileSync('mycert.cert'),
      }
    }

    app = new PatterServer(opt)

For more information on what options are available, see the [Nodejs HTTPS Docs](https://nodejs.org/api/https.html#httpscreateserveroptions-requestlistener)

## Other Functions / Objects

### PatterServer.Server
Access to the [http.server](https://nodejs.org/api/http.html#http_class_http_server) object

    app.Server

### PatterServer.devMode
Set to devMode to return errors to response

(default is 0/false)

    app.devMode = 1 // Now enabling dev mode with prints

### PatterServer.sizeLimit
The size limit for sending data to PatterServer

(default is 1e6)
    
    app.sizeLimit = 1e8 // increase size limit

### resp.notFound()
Returns a simple not found string

    resp.notFound(resp)

To modify the notFound function to send a custom not found message or page:

    resp.notFound = function(resp){...}

### PatterServer.mimeTypes
A dictionary of common mimeTypes used when sending file header content type.

Default mimetypes: 

    PatterServer.mimeTypes = {
        '.css': 'text/css',
        '.html': 'text/html',
        '.js': 'text/javascript',
        '.json': 'application/json',
        '.ico': 'image/x-icon',
        '.jpg': 'image/jpeg',
        '.png': 'image/png',
        '.svg': 'image/svg+xml',
        '.mp3': 'audio/mpeg',
        '.otf': 'application/x-font-woff',
        '.ttf': 'aplication/font-ttf',
        '.pdf': 'application/pdf',
        '.zip': 'application/zip',
    }

to add a mimetype:

    app.mimeTypes['bmp']='image/bmp'


## References
Some simple tutorials on using Node's http library that inspired patter.

- [Node HTTP](https://nodejs.org/api/http.html)
- [Node HTTPS](https://nodejs.org/api/https.html)
- [Node.js Fundamentals: Web Server Without Dependencies](https://blog.bloomca.me/2018/12/22/writing-a-web-server-node.html) by Seva Zaikov
- [Building a Node.js static file server (files over HTTP) using ES6+](https://adrianmejia.com/building-a-node-js-static-file-server-files-over-http-using-es6/#) by Adrian Mejia
