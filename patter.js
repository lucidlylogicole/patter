const version='1.2.0'
const { createServer, ServerResponse, IncomingMessage } = require("http")
const url = require('url')
const path = require('path')
const fs = require('fs')
const querystring = require('querystring')

class PatterServer {

    //---o:
    mimeTypes = {
        // maps file extention to MIME types
        '.css': 'text/css',
        '.html': 'text/html',
        '.js': 'text/javascript',
        '.json': 'application/json',
        '.ico': 'image/x-icon',
        '.jpg': 'image/jpeg',
        '.png': 'image/png',
        '.svg': 'image/svg+xml',
        '.mp3': 'audio/mpeg',
        '.otf': 'application/x-font-woff',
        '.ttf': 'aplication/font-ttf',
        '.pdf': 'application/pdf',
        '.zip': 'application/zip',
    }
    
    devMode = 0
    sizeLimit = 1e6
    
    constructor(options) {
        this.routes = {}
        this.port = 8000 // default port
        // parse options
        if (options !== undefined) {
            if (options.port) {this.port = options.port}
            if (options.ssl) {this.ssl = options.ssl}
        }
    }
    
    //---f:
    route(route,func) {
        this.routes[route]={
            func:func,
            repath: new RegExp("^" + route.replace(/::/,'(.*)').replace(/:[^\s/]+/g, '([\\w-]+)') + "$")
        }
    }
    
    //---f:
    run() {
        // Organize Routes
        this.routeList = Object.keys(this.routes).sort(function(a,b){ return b.length - a.length; })
        
        let this_ws = this
        
        // Override classes
        class PatterResponse extends ServerResponse {
            constructor() {
                super(ServerResponse)
            }
            
            //---f:
            json(obj) {
                this.setHeader("content-type", "application/json")
                this.end(JSON.stringify(obj))
            }
            //---f:
            send(args) {
                this.end(args)
            }
            
            //---f:
            sendFile(root_dir, filename) {
                // root directory, filename with relative path, response
                let this_r = this
                
                // Sanitize path
                const sanitizePath = path.normalize(decodeURI(filename)).replace(/^(\.\.[\/\\])+/, '')
                let pathname = path.join(root_dir, sanitizePath)
                
                fs.exists(pathname, function (exist) {
                    // Not Found
                    if(!exist) {
                        this_r.notFound()
                        return
                    }
                    
                    // Use index if a directory
                    if (fs.statSync(pathname).isDirectory()) {
                        pathname += '/index.html'
                    }
            
                    // read file from file system
                    fs.readFile(pathname, function(err, data){
                        if(err){
                            this_r.statusCode = 500
                            this_r.end(`Error getting the file: ${err}.`)
                            return
                        } else {
                            // Set content type based on extension
                            const ext = path.parse(pathname).ext
                            this_r.setHeader('Content-type', this_ws.mimeTypes[ext] || 'text/plain' )
                            this_r.write(data)
                            this_r.end()
                        }
                    })
                })
                
            }
    
            //---f:
            notFound() {
                this.statusCode = 404
                this.end('not found')
                if (this_ws.devMode) {
                    console.log("    NOT FOUND")
                }
            }
            //---f:
            redirect(redirect_url) {
                this.writeHead(302, {'Location':redirect_url})
                this.end()
            }

            //---f:
            setCookies(cookiesD) {
                let cookies = []
                for (let ky in cookiesD) {
                    cookies.push(`${ky}=${cookiesD[ky]}`)
                }
                this.setHeader("set-cookie", cookies)
            }

        }

        class PatterRequest extends IncomingMessage {
            constructor() {
                super(IncomingMessage)
            }

            //---f:
            getData() {
                let this_r = this
                // Get data from the request
                let promise = new Promise(function(resolve,reject){
        
                    let data = ""
                    this_r.on("data", chunk => {
                        data += chunk
                        
                        // Disconnect if too large
                        if (data.length > this_ws.size_limit) {
                            this_r.connection.destroy()
                            reject('reached size limit')
                        }
            
                    })
                    this_r.on("end", () => {
                        resolve(data)
                        return data
                    })
                })
                return promise
            }
            
            //---f:
            getFormData() {
                let this_r = this
                // Get form data from the request
                return new Promise(function(resolve,reject){
                    this_r.getData().then(data=>{
                        resolve(querystring.parse(data))
                    }).catch(e=>{
                        reject(e)
                    })
                })
            }
            
            //---f:
            getCookies() {
                let cookies = {}
                if (this.headers.cookie !== undefined) {
                    for (let cookie of this.headers.cookie.split(';')) {
                        let cind = cookie.indexOf('=')
                        let key = cookie.slice(0,cind)
                        let val = cookie.slice(cind+1)
                        cookies[key] = val
                    }
                }
                return cookies
            }
        }
        
        //---
        //---Server
        let server_options = {ServerResponse:PatterResponse, IncomingMessage:PatterRequest}
        
        let create_server = createServer
        let typ = 'HTTP'
        
        // Check for SSL and run https server
        if (this.ssl) {
            typ = "HTTPS"
            create_server = require("https").createServer
            for (let ky in this.ssl) {
                server_options[ky] = this.ssl[ky]
            }
        }
        
        //Start the Server
        console.log(`${typ} Server listening on port ${this.port}`)

        //---o:Server
        this.Server = create_server(server_options,(req, resp) => {
            if (this.devMode) {
                console.log(new Date().toLocaleTimeString(),' ',req.method, req.url)
            }
            
            // Add useful functions to request object
            req.URL = new URL(req.url,'http://localhost:'+this.port)
            req.query = req.URL.searchParams
            
            // Find the route
            let path_found = 0
            for (let i = 0, l = this.routeList.length; i < l; i++) {
                let rpath = this.routeList[i]
                let found = req.URL.pathname.match(this.routes[rpath].repath)
                if (found) { // parsed successfully
                    path_found = 1
                    try {
                        // Run Route Function
                        req.args = found.slice(1)
                        
                        // Parse route and assign variables
                        req.route = {}
                        let route_spl = rpath.split('/:').slice(1)
                        for (let r_i=0; r_i < route_spl.length; r_i++) {
                            let rte = route_spl[r_i]
                            if (rte == ':') {rte='end'}
                            req.route[rte]=req.args[r_i]
                        }
                        this.routes[rpath].func(req,resp)
                        
                    } catch (e) {
                        // Error
                        let etxt = 'ERROR: '+e.stack
                        etxt += '\n'+e.name
                        etxt += '\n'+e.message
                        console.log(etxt)
                        if (this.devMode) {
                            resp.end(etxt)
                        }
                    }
                    break
                }
            }
            
            // Route Not Found
            if (!path_found) {
                resp.notFound()
            }
            
        }).listen(this.port)

    }
    
}

module.exports = {PatterServer}
